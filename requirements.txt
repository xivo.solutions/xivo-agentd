git+https://gitlab.com/xivo.solutions/xivo-amid.git
git+https://gitlab.com/xivo.solutions/xivo-auth-client.git
git+https://gitlab.com/xivo.solutions/xivo-bus.git
git+https://gitlab.com/xivo.solutions/xivo-dao.git
git+https://gitlab.com/xivo.solutions/xivo-lib-python.git
git+https://gitlab.com/xivo.solutions/xivo-lib-rest-client.git
cherrypy==18.8.0
cheroot==9.0.0
flask-cors==3.0.10
flask-restful==0.3.9
flask==2.2.2
kombu==5.2.4
psycopg2-binary==2.9.5  # from xivo-dao
pyopenssl==23.0.0  # from xivo-lib-python
pyyaml==6.0  # from xivo-lib-python
requests==2.28.1  # from xivo-*-client
sqlalchemy==1.4.46  # from xivo-dao
future==0.18.2
python-dotenv==0.15.0 #from xivo-lib-python