#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo-agent',
    version='0.1',
    description='XiVO agent server',
    author='Avencall',
    author_email='dev@avencall.com',
    url='http://git.xivo.io/',
    license='GPLv3',
    packages=find_packages(),
    scripts=['bin/xivo-agentd'],
    package_data={
        'xivo_agent.swagger': ['*.json'],
    }
)
